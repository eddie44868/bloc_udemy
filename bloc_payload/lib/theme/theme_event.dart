part of 'theme_bloc.dart';

abstract class ThemeEvent extends Equatable {
  const ThemeEvent();

  @override
  List<Object> get props => [];
}

class ChangeThemeEvent extends ThemeEvent {
  final int radInt;
  ChangeThemeEvent({
    required this.radInt
  });
  

@override
String toString() {
    return 'ChangeThemeEvent{radInt=$radInt}';
  }

  @override
  List<Object> get props => [radInt];
}