import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

part 'counter_event.dart';
part 'counter_state.dart';

class CounterBloc extends Bloc<CounterEvent, CounterState1> {
  CounterBloc() : super(CounterState1.initial()) {
    on<IncrementCounterEvent>((event, emit) {
      emit(state.copyWith(counter: state.counter + 1));
    });

    on<DecrementCounterEvent>(_decrementCounter);
  }

  void _decrementCounter (
    DecrementCounterEvent event,
    Emitter<CounterState1> emit
  ) {
    emit(state.copyWith(counter: state.counter - 1));
  }
}
