part of 'counter_bloc.dart';

class CounterState1 extends Equatable {
  final int counter;
  CounterState1({
    required this.counter,
  });

  factory CounterState1.initial() {
    return CounterState1(counter: 0);
  }

  @override
  List<Object> get props => [counter];

  @override
  String toString() => 'CounterState1(counter: $counter)';

  CounterState1 copyWith({
    int? counter,
  }) {
    return CounterState1(
      counter: counter ?? this.counter,
    );
  }
}