import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:open_weather_cubit/repositories/weather_repository.dart';
import '../../models/custom_error.dart';
import '../../models/weather.dart';

part 'weather_cubit_state.dart';

class WeatherCubitCubit extends Cubit<WeatherState> {
  final WeatherRepository weatherRepository;
  WeatherCubitCubit({required this.weatherRepository})
      : super(WeatherState.initial());

  Future<void> fetchWeather(String city) async {
    emit(state.copyWith(status: WeatherStatus.loading));

    try {
      final Weather weather = await weatherRepository.fetchWeather(city);

      emit(state.copyWith(
        status: WeatherStatus.loaded,
        weather: weather,
      ));
    } on CustomError catch (e) {
      emit(state.copyWith(
        status: WeatherStatus.error,
        error: e,
      ));
    }
  }
}
