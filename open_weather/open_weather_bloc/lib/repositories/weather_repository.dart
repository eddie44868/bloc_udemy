// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:open_weather_cubit/exceptions/weather_exceptions.dart';
import 'package:open_weather_cubit/models/custom_error.dart';
import 'package:open_weather_cubit/services/weather_api_services.dart';
import '../models/direct_geocoding.dart';
import '../models/weather.dart';

class WeatherRepository {
  final WeatherApiServices weatherApiServices;
  const WeatherRepository({
    required this.weatherApiServices,
  });

  Future<Weather> fetchWeather(String city) async {
    try {
      final DirectGeocoding directGeocoding =
          await weatherApiServices.getDirectGeocoding(city);
      print('directGeocoding: $directGeocoding');

      final Weather tempWeather =
          await weatherApiServices.getWeather(directGeocoding);
      print('tempWeather: $tempWeather');

      final Weather weather = tempWeather.copyWith(
          name: directGeocoding.name, country: directGeocoding.country);

      return weather;
    } on WeatherException catch (e) {
      throw CustomError(errmsg: e.message);
    } catch (e) {
      throw CustomError(errmsg: e.toString());
    }
  }
}
