part of 'temp_settings_cubit.dart';

enum TempUnit {
  celcius,
  fahrenheit
}

class TempSettingsState {
  final TempUnit tempUnit;
    TempSettingsState({
    this.tempUnit = TempUnit.celcius
  });

  factory TempSettingsState.initial() {
    return TempSettingsState();
  }

  @override
  List<Object?> get props => [tempUnit];


  @override
  String toString() {
    return 'TempSettingsState{tempUnit=$tempUnit}';
  }
  TempSettingsState copyWith({
    TempUnit? tempUnit    
  }) {
    return TempSettingsState(
          tempUnit: tempUnit ?? this.tempUnit
    );
  }
}
