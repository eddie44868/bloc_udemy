class CustomError {
  final String errmsg;
  const CustomError({
    this.errmsg = '',
  });

@override
List<Object?> get props => [errmsg];

@override
String toString() {
    return 'CustomError{errmsg=$errmsg}';
  }
}
