import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:todo_app/cubits/active_todo_count/active_todo_count_cubit.dart';
import 'package:todo_app/cubits/filtered_todo/filtered_todos_cubit.dart';
import 'package:todo_app/cubits/todo_filter/todo_filter_cubit.dart';
import 'package:todo_app/pages/todo_page/todos_page.dart';

import 'cubits/todo_list/todo_list_cubit.dart';
import 'cubits/todo_search/todo_search_cubit.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<TodoFilterCubit>(
          create: (context) => TodoFilterCubit(),
        ),
        BlocProvider<TodoSearchCubit>(
          create: (context) => TodoSearchCubit(),
        ),
        BlocProvider<TodoListCubit>(
          create: (context) => TodoListCubit(),
        ),
        BlocProvider<ActiveTodoCountCubit>(
            create: (context) => ActiveTodoCountCubit(
              initialActiveTodoCount: context.read<TodoListCubit>().state.todos.length,
            )
        ),
        BlocProvider<FilteredTodosCubit>(
            create: (context) => FilteredTodosCubit(
                initialTodos: context.read<TodoListCubit>().state.todos,
            )
        )
      ],
      child: MaterialApp(
          title: 'TODO',
          theme: ThemeData(
            primarySwatch: Colors.blue,
          ),
          home: const TodosPage()),
    );
  }
}
