import 'package:equatable/equatable.dart';
import 'package:uuid/uuid.dart';

enum Filter{
  all,
  active,
  completed,
}

Uuid uuid = const Uuid();

// ignore: must_be_immutable
class Todo extends Equatable {
  String id;
  final String desc;
  final bool completed;
  Todo({
    String? id,
    required this.desc,
    this.completed = false,
  // ignore: unnecessary_this
  }) : this.id = id ?? uuid.v4();

  @override
  List<Object> get props => [id, desc, completed];

@override
String toString() {
    return 'Todo{id=$id, desc=$desc, completed=$completed}';
  }
}
