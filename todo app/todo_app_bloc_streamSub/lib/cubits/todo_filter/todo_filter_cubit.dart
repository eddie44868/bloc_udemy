import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../models/todo_model.dart';

part 'todo_filter_state.dart';

class TodoFilterCubit extends Cubit<TodoFilterState> {
  TodoFilterCubit() : super(TodoFilterState.initial());

  // ignore: non_constant_identifier_names
  void ChangeFilter(Filter newFilter) {
    emit(state.copyWith(filter: newFilter));
  }
}
