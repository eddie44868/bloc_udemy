import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:todo_app/blocs/todo_filter/todo_filter_bloc.dart';
import 'package:todo_app/pages/todo_page/todos_page.dart';
import 'blocs/active_todo_count/active_todo_count_bloc.dart';
import 'blocs/filtered_todos/filtered_todos_bloc.dart';
import 'blocs/todo_list/todo_list_bloc.dart';
import 'blocs/todo_search/todo_search_bloc.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<TodoFilterBloc>(
          create: (context) => TodoFilterBloc(),
        ),
        BlocProvider<TodoSearchBloc>(
          create: (context) => TodoSearchBloc(),
        ),
        BlocProvider<TodoListBloc>(
          create: (context) => TodoListBloc(),
        ),
        BlocProvider<ActiveTodoCountBloc>(
            create: (context) => ActiveTodoCountBloc(
              initialActiveTodoCount: context.read<TodoListBloc>().state.todos.length,
              todoListBloc: BlocProvider.of<TodoListBloc>(context))),
        BlocProvider<FilteredTodosBloc>(
            create: (context) => FilteredTodosBloc(
                initialTodos: context.read<TodoListBloc>().state.todos,
                todoFilterBloc: BlocProvider.of<TodoFilterBloc>(context),
                todoSearchBloc: BlocProvider.of<TodoSearchBloc>(context),
                todoListBloc: BlocProvider.of<TodoListBloc>(context)))
      ],
      child: MaterialApp(
          title: 'TODO',
          theme: ThemeData(
            primarySwatch: Colors.blue,
          ),
          home: const TodosPage()),
    );
  }
}
