import 'package:flutter/material.dart';
import 'create_todo.dart';
import 'search_and_filter_todo.dart';
import 'show_todos.dart';
import 'todo_header.dart';

class TodosPage extends StatelessWidget {
  const TodosPage({super.key});

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 40),
          child: Column(
            children: const [
              TodoHeader(),
              CreateTodo(),
              SizedBox(height: 20,),
              SearchAndFilterTodo(),
              ShowTodos(),
            ],
          ),
        ),
      ),
    ));
  }
}
