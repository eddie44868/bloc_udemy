part of 'filtered_todos_bloc.dart';

abstract class FilteredTodosEvent extends Equatable {
  const FilteredTodosEvent();

  @override
  List<Object> get props => [];
}

class CalculateFilteredTodoEvent extends FilteredTodosEvent {
  final List<Todo> filteredTodos;
  const CalculateFilteredTodoEvent({
    required this.filteredTodos,
  });

  @override
  String toString() => 'CalculateFilteredTodoEvent(filteredTodos: $filteredTodos)';

  @override
  List<Object> get props => [filteredTodos];
}
