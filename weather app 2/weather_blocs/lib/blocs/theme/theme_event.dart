part of 'theme_bloc.dart';

abstract class ThemeEvent extends Equatable {
  const ThemeEvent();

  @override
  List<Object> get props => [];
}


class changeThemeEvent extends ThemeEvent {
  final AppTheme appTheme;
  const changeThemeEvent({
    required this.appTheme,
  });
}
