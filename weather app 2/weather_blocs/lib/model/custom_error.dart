// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:equatable/equatable.dart';

class CustomError extends Equatable {
  final String errmsg;
  const CustomError({
    this.errmsg = '',
  });

  @override
  List<Object?> get props => [errmsg];

  @override
  bool get stringify => true;
}
