// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:weather_cubit/exceptions/weather_exception.dart';
import 'package:weather_cubit/model/custom_error.dart';
import 'package:weather_cubit/model/weather.dart';
import 'package:weather_cubit/services/weather_api_services.dart';

class WeatherRepository {
  final WeatherApiServices weatherApiServices;
  const WeatherRepository({
    required this.weatherApiServices,
  });

  Future<Weather> fetchWeather(String city) async {
    try {
      final int woeid = await weatherApiServices.getWoeid(city);

      final Weather weather = await weatherApiServices.getWeather(woeid);

      return weather;
    } on WeatherException catch (e) {
      throw CustomError(errmsg: e.message);
    } catch (e) {
      throw CustomError(errmsg: e.toString());
    }
  }
}
