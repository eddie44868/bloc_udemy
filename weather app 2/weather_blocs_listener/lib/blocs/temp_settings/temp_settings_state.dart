// ignore_for_file: public_member_api_docs, sort_constructors_first
part of 'temp_settings_bloc.dart';

enum TempUnit {
  celcius,
  fahrenheit
}

class TempSettingsState extends Equatable {
  final TempUnit tempUnit;
    TempSettingsState({
    this.tempUnit = TempUnit.celcius
  });

  factory TempSettingsState.initial() {
    return TempSettingsState();
  }

  @override
  List<Object?> get props => [tempUnit];

  TempSettingsState copyWith({
    TempUnit? tempUnit    
  }) {
    return TempSettingsState(
          tempUnit: tempUnit ?? this.tempUnit
    );
  }

  @override
  bool get stringify => true;
}

