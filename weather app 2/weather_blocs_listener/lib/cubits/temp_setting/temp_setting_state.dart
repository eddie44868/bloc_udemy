// ignore_for_file: public_member_api_docs, sort_constructors_first
part of 'temp_setting_cubit.dart';

enum TempUnit {
  celcius,
  fahrenheit
}

class TempSettingState extends Equatable {
  final TempUnit tempUnit;
  TempSettingState({
    this.tempUnit = TempUnit.celcius,
  });

  factory TempSettingState.initial() {
    return TempSettingState();
  }

  @override
  List<Object?> get props => [tempUnit];

  TempSettingState copyWith({
    TempUnit? tempUnit,
  }) {
    return TempSettingState(
      tempUnit: tempUnit ?? this.tempUnit,
    );
  }

  @override
  bool get stringify => true;
}
