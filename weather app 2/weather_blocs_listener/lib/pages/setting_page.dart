import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:weather_cubit/blocs/temp_settings/temp_settings_bloc.dart';

class SettingsPage extends StatelessWidget {
  const SettingsPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Settings'),
        ),
        body: Padding(
          padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
          child: ListTile(
            title: Text('Temperature Unit'),
            subtitle: Text('Celcius / Fahrenheit (Default: Celcius)'),
            trailing: Switch(
                value: context.watch<TempSettingsBloc>().state.tempUnit ==
                    TempUnit.celcius,
                onChanged: (_) {
                  context.read<TempSettingsBloc>().add(ToggleTempUnitEvent());
                }),
          ),
        ));
  }
}
