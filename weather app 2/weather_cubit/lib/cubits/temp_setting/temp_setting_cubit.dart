import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

part 'temp_setting_state.dart';

class TempSettingsCubit extends Cubit<TempSettingState> {
  TempSettingsCubit() : super(TempSettingState.initial());

  void toggleTempUnit() {
    emit(
      state.copyWith(
          tempUnit: state.tempUnit == TempUnit.celcius
              ? TempUnit.fahrenheit
              : TempUnit.celcius),
    );
  }
}