// ignore_for_file: public_member_api_docs, sort_constructors_first
part of 'theme_cubit.dart';

enum AppTheme {
  light,
  dark
}

class ThemeState extends Equatable {
  final AppTheme appTheme;
  const ThemeState({
    this.appTheme = AppTheme.light,
  });

 factory ThemeState.initial() {
  return ThemeState();
 }

  @override
  List<Object?> get props => [appTheme];

  ThemeState copyWith({
    AppTheme? appTheme,
  }) {
    return ThemeState(
      appTheme: appTheme ?? this.appTheme,
    );
  }

  @override
  String toString() {
      return 'ThemeState{appTheme=$appTheme}';
    }

  @override
  bool get stringify => true;
}
